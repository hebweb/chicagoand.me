import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import AddressEntry from '../AddressEntry';

class Routes extends React.Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route path="/" component={AddressEntry} />
        </Switch>
      </Router>
    );
  }
}

export default Routes;
