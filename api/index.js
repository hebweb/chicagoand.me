import express from 'express';
import bodyParser from 'body-parser';
import path from 'path';

const app = express();
const router = express.Router();

const PORT = 3000;

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json());

router.get('/ward', (req, res, next) => {
  const address = req.body.address;
  // do stuff with address
  // access the google api in a callback, send the response
  res.json({ ward: 'ward whatever' });
});

app.use('/api', router);

// serve static files (html, js, css, images, etc)
app.use(express.static(path.join(__dirname, '../public'), {
  index: 'index.html',
  redirect: false,
}));

// serve the index.html over all unmatched Routes.js
app.get('*', (req, res) => {
  res.status(200).sendFile(path.join(__dirname, '../public/index.html'));
});

app.listen(PORT, () => {
  console.log(`The magic happens on port ${PORT}`);
});
